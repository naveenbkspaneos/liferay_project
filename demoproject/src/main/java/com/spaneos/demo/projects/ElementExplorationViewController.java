package com.spaneos.demo.projects;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.spaneos.demo.project.pojo.Employee;

@Controller
@RequestMapping("VIEW")
public class ElementExplorationViewController {

	private static Log log = LogFactoryUtil
			.getLog(ElementExplorationViewController.class);

	/**
	 * By deafult, This method will render/call during deployment
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @return
	 */
	@RenderMapping
	public String handlerFirstRequest(RenderRequest req, RenderResponse res,
			Model model) {

		return "index";

	}

	@RenderMapping(params = "action=renderOne")
	public String handlerSecondRequest(RenderRequest req, RenderResponse res,
			Model model) {

		return "hello";

	}

	@RenderMapping(params = "action=renderAfterAction")
	public String testRenderMethod(RenderRequest request,
			RenderResponse response) {
		log.info("In renderAfterAction method");
		return "renderAfterAction";
	}

	@ActionMapping(params = "action=actionOne")
	public void actionOneMethod(ActionRequest request, ActionResponse response,
			@ModelAttribute("Employee") Employee emp) {
		String userName = ParamUtil.get(request, "userName", "");

		log.info("userName is " + userName);
		// response.setRenderParameter("action", "renderAfterAction");
	}

}
